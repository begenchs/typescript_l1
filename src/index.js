"use strict";
exports.__esModule = true;
var Interfaces_1 = require("./Interfaces");
var isOpen = false;
var myName = 'bega';
var myAge = 32;
var list = [1, 4, 5];
var me = ['bega', 32, false];
var job = Interfaces_1["default"].WebDev;
var phone = 'Pixel';
var tablet = 2;
// Functions in TypeScript
// ? for optional params
// const sayHello = (word?: string): string => {
//   console.log(word || 'Hello')
//   return word || 'Hello'
// }
// sayHello('veg')
// Default params
// Rest params work as expected
var sayWord = function (word) {
    if (word === void 0) { word = 'Hello'; }
    var otherStuff = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        otherStuff[_i - 1] = arguments[_i];
    }
    console.log(otherStuff);
    return word;
};
sayWord('veg', 'bega');
// Implicit Types in TS
var newName = 'Bega';
newName = 'Sega';
newName = 45;
newName = false;
console.log(newName);
// Gets type from initial declaration
var newNameTwo = newName;
newNameTwo = true;
// Union Types with |
var makeMargin = function (x) {
    return "margin: ".concat(x, "px");
};
makeMargin(19);
makeMargin(false);
makeMargin('Bee');
// Null Types
var dog = 'Sammy';
dog = undefined;
dog = 'Lucie';
dog = 10;
dog = null;
// Interface
var sayName = function (_a) {
    var name = _a.name, age = _a.age;
    console.log(name);
    return { name: name, age: age };
};
sayName({
    name: 'bega',
    age: 32
});
sayName({
    age: 32,
    name: ''
});
// Enums
// Numeric Enum
var Type;
(function (Type) {
    Type[Type["Video"] = 0] = "Video";
    Type[Type["BlogPost"] = 1] = "BlogPost";
    Type[Type["Quiz"] = 2] = "Quiz";
})(Type || (Type = {}));
var createContent = function (contentType) { };
createContent(Type.Quiz);
console.log(Type.Quiz);
// String Enum
var createContent2 = function (contentType) { };
createContent2(Interfaces_1.Type2.Quiz);
console.log('Type2', Interfaces_1.Type2.Quiz);
// Classes
var Team = /** @class */ (function () {
    // public teamName: string; this is same as above
    // private teamName: string; prevents outside usage
    // readonly teamName: string; prevents from being changed
    function Team(teamName) {
        this.teamName = teamName;
    }
    Team.prototype.score = function () {
        this.teamName = 'changing';
        console.log('GOAAAT');
        return 'GOOAAL';
    };
    return Team;
}());
var fcb = new Team('Barca');
fcb.score();
fcb.teamName;
