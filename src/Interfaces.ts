export interface Person {
  name: string; // ? optional param
  age?: number;
}

export enum Type2 {
  Video = 'VIDEO', // 0
  BlogPost = 'BLOG', // 1
  Quiz = 'QUIZ', // 2
}

enum Job {
  WebDev,
  WebDesigner,
  PM,
}

export default Job