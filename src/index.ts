import Job, { Person, Type2 } from './Interfaces';

const isOpen: boolean = false;

const myName: string = 'bega';

const myAge: number = 32;

const list: number[] = [1, 4, 5];

const me: [string, number, boolean] = ['bega', 32, false];

const job: Job = Job.WebDev;

const phone: any = 'Pixel';
const tablet: any = 2;

// Functions in TypeScript
// ? for optional params

// const sayHello = (word?: string): string => {
//   console.log(word || 'Hello')
//   return word || 'Hello'
// }

// sayHello('veg')
// Default params
// Rest params work as expected
const sayWord = (word = 'Hello', ...otherStuff: string[]): string => {
  console.log(otherStuff);
  return word;
};

sayWord('veg', 'bega');

// Implicit Types in TS
let newName: string | number | boolean = 'Bega';
newName = 'Sega';
newName = 45;
newName = false;
console.log(newName);

// Gets type from initial declaration
let newNameTwo = newName;
newNameTwo = true;

// Union Types with |
const makeMargin = (x: string | number | boolean): string => {
  return `margin: ${x}px`;
};

makeMargin(19);
makeMargin(false);
makeMargin('Bee');

// Null Types

let dog: string | undefined | null | number = 'Sammy';
dog = undefined;
dog = 'Lucie';
dog = 10;
dog = null;

// Interface

const sayName = ({ name, age }: Person): Person => {
  console.log(name);
  return { name, age };
};

sayName({
  name: 'bega',
  age: 32,
});

sayName({
  age: 32,
  name: '',
});

// Enums

// Numeric Enum

enum Type {
  Video, // 0
  BlogPost, // 1
  Quiz, // 2
}

const createContent = (contentType: Type) => {};
createContent(Type.Quiz);
console.log(Type.Quiz);

// String Enum

const createContent2 = (contentType: Type2) => {};

createContent2(Type2.Quiz);

console.log('Type2', Type2.Quiz);

// Classes

class Team {
  teamName: string;
  // public teamName: string; this is same as above
  // private teamName: string; prevents outside usage
  // readonly teamName: string; prevents from being changed

  constructor(teamName) {
    this.teamName = teamName;
  }

  score(): string {
    this.teamName = 'changing';
    console.log('GOAAAT');
    return 'GOOAAL';
  }
}

const fcb = new Team('Barca');

fcb.score();
fcb.teamName;

// Generics

const outputInput = <T>(arg: T): T => {
  return arg;
};

outputInput('HI');
outputInput(3);

// Duck Typing
class Dancer implements Person {
  name: string;
  age: number;
}

let ElNino: Person = new Dancer();

const fake = {
  name: 'See',
};

ElNino = fake;
